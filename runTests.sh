#!/bin/bash

python - <<EOF
import sys
if sys.version_info.major>2:
    sys.exit(0)
if sys.version_info.minor<7:
    sys.exit(1)
EOF
if [[ "$?" == 1 ]]; then
    export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"

    lsetup python
    lsetup git
else
    python --version
fi

if ! [[ -z "$CI_JOB_ID" ]]; then
    echo "CI job: $CI_JOB_ID"
    git config --global user.email "Balthasar.Maria.Schachtner@cern.ch"
    git config --global user.name "Balthasar Schachtner"
else
    echo "local job"
fi

export PYTHONPATH="$PYTHONPATH:$(readlink -f ..)"
echo $PYTHONPATH
python -m commonHelpers.tests
RES="$?"
if [[ $RES != 0 ]]; then
    exit $RES
fi
# also test python2.7 is python is version 3
python - <<EOF
import sys
if sys.version_info.major>2:
    sys.exit(1)
EOF
if [[ "$?" == 1 ]]; then
    echo "####"
    echo "# Also test python2.7 #"
    echo "####"
    python2.7 -m commonHelpers.tests
    RES="$?"
fi
exit $RES
