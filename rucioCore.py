# -*- coding: utf-8 -*-
import logging
logging.basicConfig()
logger = logging.getLogger(__name__)

databaseDirectory = "/dev/shm/rucioUtils/"

import re

try:
    from rucio import client
    from rucio.client import Client
    from rucio.common.exception import DatabaseException
except ImportError:
    logger.error("Note: To use rucio functions you have to setup rucio within an slc6 environment!")
    raise
c = Client()

from memeMock import cache

from commonHelpers.sample import Sample

def protectDatabase(f):
    def protector(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except DatabaseException:
            c = Client()
            logger.info("refresh client")
            return f(*args, **kwargs)
    return protector


@cache(cacheDir=databaseDirectory, ttl=6000)
def listFiles(DID):
    sample = Sample(DID)
    return [f for f in c.list_files(sample.scope(), sample.DID())]

@cache(cacheDir=databaseDirectory, ttl=10000)
def wildcardedSearch(wildcard):
    sample = Sample(wildcard)
    return [item for item in c.list_dids(sample.scope(), {'name': wildcard})]

@cache(cacheDir=databaseDirectory, ttl=10000)
def replicaDicts(did):
    sample = Sample(did)
    d = {'scope': sample.scope(), 'name':sample.DID()}
    try:
        return [ item for item in c.list_replicas([d], schemes=['root'], all_states=True, unavailable=True)]
    except ValueError:
        logger.error("failed to get replica dict for {}".format(d))
        raise

@cache(cacheDir=databaseDirectory, ttl=6000)
def getUserSubscriptions(user):
    """
    Rules are dictionaries with this information:
    u'locks_ok_cnt': 1, 
    u'source_replica_expression': None, 
    u'weight': None, 
    u'purge_replicas': False, 
    u'rse_expression': u'BNL-OSG2_USERDISK', 
    u'updated_at': datetime.datetime(2016, 9, 14, 1, 31, 51), 
    u'child_rule_id': None, 
    u'id': u'346b03bb0b9947059b6590d21b9cfd46', 
    u'ignore_account_limit': False, 
    u'error': None, u'locks_stuck_cnt': 0, 
    u'locks_replicating_cnt': 0, 
    u'notification': u'NO', 
    u'copies': 1, 
    u'comments': None, 
    u'priority': 3, 
    u'state': u'OK', 
    u'scope': u'user.bschacht', 
    u'subscription_id': None, 
    u'stuck_at': None, 
    u'ignore_availability': True, 
    u'eol_at': None, 
    u'expires_at': None, 
    u'did_type': u'DATASET', 
    u'account': u'bschacht', 
    u'locked': False, 
    u'name': u'user.bschacht.0066.v026-1MC15_AA_w.SUSYTauAnalysisCycle.mc15_363437.e4715_s2726_r7725_r7676_p2666_SUSYTauAnalysis.log.96579338', 
    u'created_at': datetime.datetime(2016, 9, 14, 1, 5, 14), 
    u'activity': u'User Subscriptions', 
    u'grouping': u'DATASET'
    """
    rules = []
    for rule in c.list_account_rules(user):
        rules.append(rule)
    return rules

@protectDatabase
@cache(cacheDir=databaseDirectory, ttl=6000)
def rootPaths(did, rse='LRZ-LMU_LOCALGROUPDISK', onlyFullyAvailable=False):
    paths = []

    repDicts = replicaDicts(did)
    # sometimes we get multiple dicts, first filter the ones with rse in there
    matchedDicts = []
    for repDict in repDicts:
        if rse in repDict['states']:
            matchedDicts.append(repDict)
    if len(matchedDicts) < 1:
        logger.error("Couldn't find a dict of {} on rse {}, skipping it!".format(did, rse))
        return []

    for repDict in matchedDicts:
        state = repDict['states'][rse]
        if state != 'AVAILABLE':
            logger.warning("{} on {} in state {}".format(did, rse, state))
            if onlyFullyAvailable:
                return []
        try:
            for path in repDict['rses'][rse]:
                path = re.sub('root://xrootd-lrz-lmu.grid.lrz.de:1094/+', 'root://lcg-lrz-rootd.grid.lrz.de/', path)
                paths.append(path)
        except KeyError:
            logger.error("couldn't find path on {} for {}".format(rse, did))
            logger.info("rucio dict: {}".format(repDict))
            raise
    
    return list(set(paths))


if __name__ == '__main__':

    assert set(rootPaths('user.bschacht.0001.v020-87Data_FF.SUSYTauAnalysisCycle.data15_00276262.f620_m1480_p2425_SUSYTauAnalysis.root')) == set([u'root://lcg-lrz-rootd.grid.lrz.de:1094/pnfs/lrz-muenchen.de/data/atlas/dq2/atlaslocalgroupdisk/rucio/user/bschacht/cf/5f/user.bschacht.8356837._000001.SUSYTauAnalysis.root', u'root://lcg-lrz-rootd.grid.lrz.de:1094/pnfs/lrz-muenchen.de/data/atlas/dq2/atlaslocalgroupdisk/rucio/user/bschacht/f9/73/user.bschacht.8356837._000003.SUSYTauAnalysis.root']), rootPaths('user.bschacht.0001.v020-87Data_FF.SUSYTauAnalysisCycle.data15_00276262.f620_m1480_p2425_SUSYTauAnalysis.root')

    print (rootPaths("user.bschacht.0006.v029-5MC15_TTa.SUSYTauAnalysisCycle.mc15_361068.e3836_s2608_s2183_r7725_r7676_p2949_Metadata.root"))
    print('passed tests!')
