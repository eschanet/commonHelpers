import collections
import re
import logging
logger = logging.getLogger(__name__)

####
# list
####
def splitBySelectors(container, selectorDict, matchAll=True, addElse=False):
    """Get a container, use a dict of lambdas to split

    matchAll to raise an exception for unmatched entries
    addElse: introduce an key "else" for unmatched entries
    """
    splitDict = {}
    for item in container:
        matches = []
        for selectorName, selector in selectorDict.items():
            if selector(item):
                if matches:
                    raise ValueError("item {} has already been matched: {}, will be matched again by: {}".format(item, matches, selectorName))
                splitDict.setdefault(selectorName, []).append(item)
                matches.append(selectorName)
        if addElse and len(matches) == 0:
            splitDict.setdefault("else", []).append(item)
            matches.append("else")
            
        if matchAll and len(matches) == 0:
            raise IndexError("Didn't match {}".format(item))
    return splitDict

def splitByPatterns(container, patterns, matchAll=True):
    outDict = {}
    nMatches = 0
    for pattern in patterns:
        outDict[pattern] = []
        for item in container:
            search = re.search(pattern, item)
            if search is not None:
                nMatches += 1
                outDict[pattern].append(item)

    if not checkUniqueValues(outDict):
        raise KeyError

    if nMatches != len(container) and matchAll:
        invDict = invertDict(outDict)
        print(invDict)
        for f in set(container) - set(getValues(outDict)):
            logger.warning("some samples not matched: {}".format(f))
        raise KeyError
    return outDict

def listDiff(cont1, cont2):
    set1 = set(cont1)
    set2 = set(cont2)
    myDict = {}
    myDict['xor'] = set1^set2
    myDict['and'] = set1&set2
    myDict['1-2'] = set1-set2
    myDict['2-1'] = set2-set1
    return myDict
####
# dictionary
####
def getValues(myDict):
    allValues = []
    for key in myDict:
        allValues += myDict[key]
    return allValues

def invertDict(myDict):
    invDict = {}
    for key in myDict:
        val = myDict[key]
        if type(val) is not list:
            val = [val]

        for v in val:
            if invDict.has_key(v):
                invDict[v].append(key)
            else:
                invDict[v] = [key]
    return invDict

def checkUniqueValues(myDict):
    ''' check if all the values in the dictionary are different '''
    allValues = getValues(myDict)
    counter = collections.Counter(allValues)
    duplicates = []
    for val in counter:
        if counter[val] > 1:
            print(val)
            duplicates.append(val)
    if len(duplicates) > 0:
        invDict = invertDict(myDict)
        logger.warning("duplicates found:")
        for duplicate in duplicates:
            logger.warning("duplicate {}: {}".format(duplicate,invDict[duplicate]))
        return False
    else:
        return True

####
# iterables
####

# https://docs.python.org/3.1/library/itertools.html#recipes
import itertools
try:
    from itertools import izip_longest as zip_longest
except ImportError:
    from itertools import zip_longest as zip_longest

def grouper(n, iterable, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)
