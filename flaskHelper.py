from flask import request
from commonHelpers.html.htmlWriter import Tag

def addAction(func, name=None, text=None, args=[], kwargs={}):
    """ Add a button to call the function <func>

    Returns html and catches the request
    Note that the parent function needs the post method"""
    if name is None:
        name = func.__name__
    if text is None:
        text = name

    if request.method == 'POST':
        if request.form.get(name) == text:
            return str(func(*args, **kwargs))
    f = Tag("form", Tag('input', type="submit", _name=name, value=text), method="POST", id=name)
    return str(f)
