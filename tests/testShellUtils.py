#!/usr/bin/env python
import unittest
import os, shutil
import uuid
import logging
logger = logging.getLogger(__name__)

import commonHelpers.shellUtils as SU

class TestShellUtils(unittest.TestCase):

    def test_hostname(self):
        print("hostname", SU.hostname())

    def test_cd(self):
        logger.info("test cd into dir")
        with SU.cd("/tmp") as tmp:
            self.assertTrue(os.getcwd() == "/tmp")
        logger.info("test cd into non-existing dir")
        self.assertRaises(OSError, SU.cd("/tmp/{}".format(str(uuid.uuid4()))).__enter__)
        logger.info("test cd from nonexisting dir")
        testDir = "/tmp/{}".format(str(uuid.uuid4()))
        os.makedirs(testDir)
        with SU.cd(testDir):
            shutil.rmtree(testDir)
            with SU.cd("/tmp"):
                pass
        logger.info("test cd when dir is removed")
        testDir = "/tmp/{}".format(str(uuid.uuid4()))
        os.makedirs(testDir)
        with SU.cd(testDir):
            with SU.cd("/tmp"):
                shutil.rmtree(testDir)

    def test_programAvailable(self):
        self.assertTrue(SU.programAvailable('python'))
        self.assertTrue(not SU.programAvailable('aasdfhjasdflhkasdfh'))

if __name__ == '__main__':
    unittest.main()
