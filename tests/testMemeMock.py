#!/usr/bin/env python
import unittest
import os, shutil
import uuid
import atexit
import logging
logger = logging.getLogger(__name__)

from commonHelpers.memeMock import cache
from commonHelpers.shellUtils import cdTmp

class TestMemeMock(unittest.TestCase):

    def test_memeMock(self):
        with cdTmp():
            @cache()
            def test(**kwargs):
                return kwargs

            self.assertDictEqual(test(), {})
            self.assertDictEqual(test(x=5), {"x":5})
            self.assertDictEqual(test(refreshCache=True), {})
            # if we wait until the program finishes cwd could have changed...
            atexit._run_exitfuncs()
            print("finished")

if __name__ == '__main__':
    unittest.main()
