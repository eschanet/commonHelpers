#!/usr/bin/env python
import unittest
from contextlib import contextmanager
import logging
logger = logging.getLogger(__name__)

import commonHelpers.shellUtils as SU
from commonHelpers.git import Git

# Helpers
@contextmanager
def gitWithCommits():
    with SU.tmpDir() as tmpDir:
        g = Git(tmpDir)
        g.init()
        newFile = tmpDir + '/test.txt'
        with open(newFile, 'w') as f:
            f.write('test')
        g.add(newFile)
        g.commit("test1")
        yield g

class TestGit(unittest.TestCase):

    def test_isGit(self):
        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            self.assertTrue(not g.isGit())
        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            self.assertTrue(g.isGit())

    def test_branch(self):
        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            self.assertEqual(g.currentBranch(), "master")

    def test_add(self):
        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            newFile = tmpDir + '/test.txt'
            with open(newFile, 'w') as f:
                f.write('test')
            g.add(newFile)
            print(g.status())

    def test_commit(self):
        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            newFile = tmpDir + '/test.txt'
            with open(newFile, 'w') as f:
                f.write('test')
            g.add(newFile)
            g.commit("test1")
            print(g.status())

    def test_status(self):
        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            print(g.status())
        # needs many more tests...

    def test_currentCommit(self):
        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            logger.info('no commit: {}'.format(g.currentCommit()))

        with gitWithCommits() as g:
            logger.info('with commit: {}'.format(g.currentCommit()))

    def test_fetchAll(self):
        with SU.tmpDir() as remoteDir:
            gp = Git(remoteDir)
            gp.init()

        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            g.fetchAll()

    def test_lastFetch(self):
        # fixme, need remote git here
        with SU.tmpDir() as remoteDir:
            gp = Git(remoteDir)
            gp.init()

        with SU.tmpDir() as tmpDir:
            g = Git(tmpDir)
            g.init()
            g.lastFetch()

if __name__ == '__main__':
    unittest.main()
