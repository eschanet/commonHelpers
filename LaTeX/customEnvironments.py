# -*- coding: utf-8 -*-
from commonHelpers.interaction import tableFromDicts

from .LaTeXWriter import Environment, macro
from .helper import cleanLaTeX
# custom environments
class Letter(Environment):
    def __init__(self, adress, dw=None, subject='', opening='', closing='Mit freundlichen Grüßen,'):
        super(Letter, self).__init__("letter", adress, dw=dw)

        self.add(macro("subject", subject))
        self.add(macro("opening", opening))

        self.closing = closing

    def __exit__(self, *args):
        self.add(macro("closing", self.closing))
        return super(Letter, self).__exit__(*args)

class Tabular(Environment):
    def __init__(self, rows=[], grid=False):
        super(Tabular, self).__init__("tabular")

        self.rowLength = None
        self.grid = grid
        self.customFormat = None
        if self.grid:
            self.add("\\hline")
        for row in rows:
            self.addRow(*row)

    def begin(self):
        if self.grid:
            self.args.append("|" + "|".join(["l" for i in range(self.rowLength)]) + "|")
        elif self.customFormat is not None:
            self.args.append(self.customFormat)
        else:
            self.args.append("l"*self.rowLength)
        return super(Tabular, self).begin()

    # def add(self, tex):
    #     raise NotImplementedError("tabular should not be used with add")

    def addRow(self, *items):
        if self.rowLength is None:
            self.rowLength = len(items)

        if len(items) != self.rowLength:
            raise IndexError('Only equal length rows are allowed {}/{}'.format(len(items), self.rowLength))

        line = "&".join([cleanLaTeX(str(item)) for item in items]) + '\\\\'
        if self.grid:
            self.add(line + "\\hline")
        else:
            self.add(line)

    def addMultiColumn(self, row):
        """ Add a row of style: [(nCells, content), ...] """
        if self.rowLength is None:
            # print("set row length")
            self.rowLength = sum([i for i, _ in row])

        # print( row, sum([i for i, _ in row]), self.rowLength)
        if sum([i for i, _ in row]) != self.rowLength:
            raise IndexError('Only equal length rows are allowed')
        cells = []
        for nCells, content in row:
            cells.append(macro("multicolumn", nCells, "c", content))
        self.add("&".join(cells) + '\\\\')

    def addDictionaries(self, *ds):
        self.addTopRule()
        firstLine = True
        for row in tableFromDicts(ds):
            self.addRow(*row)
            if firstLine:
                self.addMidRule()
                firstLine = False
        self.addBottomRule()

    def addTopRule(self):
        self.add(macro("toprule"))

    def addMidRule(self):
        self.add(macro("midrule"))

    def addBottomRule(self):
        self.add(macro("bottomrule"))

class Itemize(Environment):
    def __init__(self):
        super(Itemize, self).__init__("itemize")

    def addItem(self, text):
        self.add("\\item {}".format(text))

class Longtable(Tabular):
    def __init__(self, *args, **kwargs):
        super(Longtable, self).__init__(*args, **kwargs)
        self.envName = "longtable"

class Adjustbox(Environment):
    def __init__(self, maxWidth="0.99\\textwidth", maxHeight="0.8\\textheight"):
        super(Adjustbox, self).__init__("adjustbox", "max width={}, max totalheight={}".format(maxWidth, maxHeight))

class Frame(Environment):
    def __init__(self, dw=None, title=None):
        super(Frame, self).__init__("frame")
        if title is not None:
            self.add(macro("frametitle", title))

class Columns(Environment):
    def __init__(self, dw=None):
        super(Columns, self).__init__("columns", dw=dw)

class Column(Environment):
    def __init__(self, dw=None, width="0.48\\textwidth"):
        super(Column, self).__init__("column", width, dw=dw)

# custom macros
def includeGraphics(path, ignoreNone=False, width="\\textwidth", height="\\textheight", keepaspectratio=True):
    import os.path
    if path is None and ignoreNone:
        return " "
    if not os.path.exists(path):
        raise IOError
    return macro("includegraphics", os.path.abspath(path), width=width, height=height, keepaspectratio=keepaspectratio)

def vspace(i, unit="em"):
    return macro("vspace", "{}{}".format(i, unit))
