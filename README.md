# Todo
 - Include Docker image with uncertainties module installed

# Git commands
[alias]
    sPull = "subtree pull --prefix=commonHelpers commonHelpers master"
    sPush = "subtree push --prefix=commonHelpers commonHelpers master"
