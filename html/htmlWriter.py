import os

from contextlib import contextmanager
from collections import OrderedDict

from commonHelpers.pathHelpers import ensurePathExists
from commonHelpers.interaction import tableFromDicts
from commonHelpers.pythonHelpers import string_types

def indent(text):
    return " " + "\n ".join(text.split("\n"))

class HtmlWriter(object):
    def __init__(self, htmlPath="/tmp/tmp.tex", declaration="<!DOCTYPE html>", title=None):

        self.htmlPath = htmlPath
        if self.htmlPath is not None and self.htmlPath [-5:] != '.html':
            self.htmlPath += ".html"

        self.title = title

        self._declaration = declaration
        self._head = ""
        self._body = ""

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            return
        
        if self.htmlPath is None:
            return

        ensurePathExists(os.path.dirname(self.htmlPath))
        with open(self.htmlPath, "w") as f:
            f.write(self.getHtml())

        return self.htmlPath

    def __str__(self):
        return self.getHtml()

    @contextmanager
    def head(self):
        h = Tag("head")
        if self.title is not None:
            h('title', self.title)
        yield h
        self._head = str(h) + "\n"

    @contextmanager
    def body(self):
        b = Tag("body")
        yield b
        self._body = str(b) + "\n"

    def getHtml(self):
        if not self._head and self.title:
            with self.head():
                pass
        return "\n".join([self._declaration, self._head, self._body])

class Tag(object):
    """Tag object translates into html code"""
    def __init__(self, name, content=None, **attributes):
        self.name = name
        if content is None:
            self.content = []
        else:
            self.content = [content]
        self.attributes = attributes
        for attribute, val in self.attributes.items():
            if attribute == "_name":
                self.attributes["name"] = val
                del(self.attributes["_name"])

    def __str__(self):
        s = "<{}".format(self.name)
        for key, val in self.attributes.items():
            s += ' {}="{}"'.format(key,val)
        s += ">"

        if len(self.content) > 1:
            s += "\n"
            for cont in self.content:
                s += indent(str(cont)) + "\n"
        elif len(self.content) == 1:
            s+= str(self.content[0])

        if len(self.content) > 0:
            s += "</{}>".format(self.name)
        return s

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is not None:
            return

    def __call__(self, tag=None, *args, **kwargs):
        """Calling a Tag returns a new Tag"""
        if isinstance(tag, string_types):
            t = Tag(tag, *args, **kwargs)
        elif issubclass(tag, Tag):
            t = tag(*args, **kwargs)
        else:
            raise TypeError("don't know what to do with {}".format(tag))
        self.add(t)
        return t
        
    def add(self, cont):
        """Add content here, for creation of tags str(cont) will be called"""
        self.content.append(cont)

class Table(Tag):
    def __init__(self, *rows):
        super(Table, self).__init__("table")
        for row in rows:
            self.addRow(row)

    def addDictionaries(self, *ds):
        # self.rows += tableFromDicts(ds)
        for row in tableFromDicts(ds):
            self.addRow(*row)

    def addRow(self, *row):
        tr = Tag('tr')
        for item in row:
            tr("td", item)
        self.add(tr)

    def addColumns(self, *columns):
        length = max([len(c) for c in columns])
        for i in range(length):
            row = []
            for column in columns:
                try:
                    row.append(column[i])
                except IndexError:
                    row.append("")
            self.addRow(*row)

class Link(Tag):
    def __init__(self, text, link, **kwargs):
        super(Link, self).__init__("a", text, href=link, **kwargs)

class Color(Tag):
    def __init__(self, text, color="color:red", **kwargs):
        super(Color, self).__init__("span", text, style=color, **kwargs)
