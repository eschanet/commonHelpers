#!/usr/bin/env python
from collections import OrderedDict

from htmlWriter import HtmlWriter, Table

with HtmlWriter("example.html") as H:
    # with H.head() as h:
    #     pass
    with H.body() as b:
        b("h1", "testpage")
        with b("p") as p:
            p.add("mehl")
            p("br")
            p.add("lehm")

        b("h1", "Test tables")
        b("h2", "from list of lists")
        with b("p") as p:
            p.add(Table(*[["", 'row'], ["1", "cont1"]]))
        b("h2", "from list of ordered dicts")
        ds = []
        for i in range(4):
            d = OrderedDict()
            for j in range(19):
                d['row {}'.format(j)] = i+j
            ds.append(d)
        with b("p") as p:
            t = Table()
            t.addDictionaries(*ds)
            p.add(t)
